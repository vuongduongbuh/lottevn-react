import React from "react";
import { Route } from "react-router-dom";
import Home from "./modules/home/home";
import Category from "./modules/category/category";
import CheckoutCart from "./modules/checkout/cart";

export default class AppRouter extends React.Component {
    render() {
        return (
            <div>
                <Route exact path="/" component={Home} />
                <Route path="/category" component={Category} />
                <Route path="/checkout/cart" component={CheckoutCart} />
            </div>
        )
    }
}

