import { appConstant } from '../App.constant';

export const fetchGuest = (url, type, data = null) => {
    return new Promise((resolve, reject) => {
        return fetch(appConstant.API_ENDPOINT + url, {
            method: type,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${appConstant.TOKEN.GUEST}`
            },
            body: data ? JSON.stringify(data) : ''
        }).then((res) => {
            resolve(res.json());
        }).catch((err) => {
            reject(err);
        })
    })
}

export const fetchAuth = (url, type, data = null) => {
    const token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        const params = {
            method: type,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: data ? JSON.stringify(data) : null
        }
        if (!params.data) {
            delete params.body;
        }
        return fetch(appConstant.API_ENDPOINT + url, params).then((res) => {
            resolve(res.json());
        }).catch((err) => {
            reject(err);
        })
    })
}

// export const fetchElS = () => {
//     return new Promise((resolve, reject) => {
//         return fetch(appConstant.API_ENDPOINT + `integration/customer/token/custom`, {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': 'Bearer '
//             },
//             body: JSON.stringify(data)
//         }).then((res) => {
//             resolve(res.json());
//         }).catch((err) => {
//             reject(err);
//         })
//     })
// }

