/**
 * @file
 * Provides some feature.
 *
 * Created by anhuy on 10/10/17.
 */
import createHistory from 'history/createBrowserHistory';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk';

import categoryReducer from './category/category.reducer';
import homeReducer from './home/home.reducer';
import authReducer from './auth/auth.reducer';

export const LotteReducer = combineReducers({
    home: homeReducer,
    auth: authReducer,
    category: categoryReducer,
    router: routerReducer
})

const logger = createLogger({

});

export const history = createHistory();
const middleware = routerMiddleware(history)

export const store = createStore(LotteReducer, compose(applyMiddleware(thunk, middleware, logger)))