/**
 * @providesModule categoryActions
 */

import { createAction } from 'redux-act';
import { fetchGuest, fetchAuth } from '../../services/fetch';

export const AUTH_LOGIN = createAction('AUTH_LOGIN')
export const AUTH_LOGIN_SUCCESS = createAction('AUTH_LOGIN_SUCCESS')
export const AUTH_LOGIN_FAILURE = createAction('AUTH_LOGIN_FAILURE')

export const AUTH_LOGOUT = createAction('AUTH_LOGOUT')
export const AUTH_LOGOUT_SUCCESS = createAction('AUTH_LOGOUT_SUCCESS')
export const AUTH_LOGOUT_FAILURE = createAction('AUTH_LOGOUT_FAILURE')

export const AUTH_LOAD_INFO = createAction('AUTH_LOAD_INFO')
export const AUTH_LOAD_INFO_SUCCESS = createAction('AUTH_LOAD_INFO_SUCCESS')
export const AUTH_LOAD_INFO_FAILURE = createAction('AUTH_LOAD_INFO_FAILURE')

export const loadUserInfo = () => {
    return (dispatch) => {
        dispatch(AUTH_LOAD_INFO());
        return fetchAuth('customers/me/custom', 'GET').then((res) => {
            localStorage.setItem('userInfo', JSON.stringify(res));
            dispatch(AUTH_LOAD_INFO_SUCCESS(res));
        }).catch((err) => {
            dispatch(AUTH_LOAD_INFO_FAILURE(err))
        })
    }
}

export const login = (data) => {
    return (dispatch) => {
        dispatch(AUTH_LOGIN());
        return fetchGuest('integration/customer/token/custom', 'POST', data).then((token) => {
            localStorage.setItem('token', token);
            dispatch(AUTH_LOGIN_SUCCESS(token));
        }).catch((err) => {
            dispatch(AUTH_LOGIN_FAILURE(err))
        })
    }
}

export const logout = () => {
    return (dispatch) => {
        dispatch(AUTH_LOGOUT());
        return fetchAuth('integration/customer/logout', 'GET').then((token) => {
            localStorage.removeItem('token');
            localStorage.removeItem('userInfo');
            dispatch(AUTH_LOGOUT_SUCCESS(token));
        }).catch((err) => {
            dispatch(AUTH_LOGOUT_FAILURE(err))
        })
    }
}