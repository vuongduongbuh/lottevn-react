import * as auth from './auth.actions';
import { createReducer } from 'redux-act';

const initialState = {
    loading: false,
    isLoggedIn: localStorage.getItem('token') ? true : false,
    error: {},
    userInfo: localStorage.getItem('userInfo') ? JSON.parse(localStorage.getItem('userInfo')) : {}
}

const handlers = {
    [auth.AUTH_LOGIN]: (state) => ({
        ...state,
        loading: true
    }),
    [auth.AUTH_LOGIN_SUCCESS]: (state, payload) => ({
        ...state,
        loading: false,
        isLoggedIn: true
    }),
    [auth.AUTH_LOGIN_FAILURE]: (state, err) => ({
        ...state,
        loading: false,
        error: err
    }),
    [auth.AUTH_LOGOUT]: (state) => ({
        ...state,
        loading: true
    }),
    [auth.AUTH_LOGOUT_SUCCESS]: (state, payload) => ({
        ...state,
        loading: false,
        isLoggedIn: false
    }),
    [auth.AUTH_LOGOUT_FAILURE]: (state, err) => ({
        ...state,
        loading: false,
        error: err
    }),
    [auth.AUTH_LOAD_INFO]: (state) => ({
        ...state,
        loading: true
    }),
    [auth.AUTH_LOAD_INFO_SUCCESS]: (state, payload) => ({
        ...state,
        loading: false,
        userInfo: payload
    }),
    [auth.AUTH_LOAD_INFO_FAILURE]: (state, err) => ({
        ...state,
        loading: false,
        error: err
    })
}

const reducer = createReducer(handlers, initialState);

export default reducer