import * as home from './home.actions';
import { createReducer } from 'redux-act';

const initialState = {
    loading: false,
    products: [],
    error: {}
}

const handlers = {
    [home.LOAD_PRODUCTS]: (state) => ({
        ...state,
        loading: true
    }),
    [home.LOAD_PRODUCTS_SUCCESS]: (state, payload) => ({
        ...state,
        loading: false,
        products: payload
    }),
    [home.LOAD_PRODUCTS_FAILURE]: (state, err) => ({
        ...state,
        loading: false,
        error: err
    })
}

const reducer = createReducer(handlers, initialState);

export default reducer