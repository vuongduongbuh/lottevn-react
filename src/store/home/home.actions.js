/**
 * @providesModule categoryActions
 */

import { createAction } from 'redux-act'

export const LOAD_PRODUCTS = createAction('LOAD_PRODUCTS')
export const LOAD_PRODUCTS_SUCCESS = createAction('LOAD_PRODUCTS_SUCCESS')
export const LOAD_PRODUCTS_FAILURE = createAction('LOAD_PRODUCTS_FAILURE')

// export const getCategoryList = () => {
//     return (dispatch) => {
//         dispatch(LOAD_PRODUCTS())
//         var url = constants.api.categories
//         var params = {
//             method: 'GET'
//         }
//         return fetchLMS(url, params)
//             .then((responseJson) => {
//                 var categories = responseJson.data
//                 localStorage.setItem('categories', JSON.stringify(categories))
//                 window.lotteCategoryList = JSON.stringify(categories)
//                 dispatch(LOAD_PRODUCTS_SUCCESS(categories))
//             }).catch((errMsg) => {
//                 dispatch(LOAD_PRODUCTS_FAILURE(errMsg))
//             })
//     }
// }
