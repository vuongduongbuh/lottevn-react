/**
 * @providesModule categoryActions
 */

import { createAction } from 'redux-act'

export const CAT_LOAD_PRODUCTS = createAction('CAT_LOAD_PRODUCTS')
export const CAT_LOAD_PRODUCTS_SUCCESS = createAction('CAT_LOAD_PRODUCTS_SUCCESS')
export const CAT_LOAD_PRODUCTS_FAILURE = createAction('CAT_LOAD_PRODUCTS_FAILURE')

export const loadProducts = () => {
    return (dispatch) => {
        dispatch(CAT_LOAD_PRODUCTS());

        return fetch(`https://els.lotte.vn/api/v1/categories/tree?filter={"where": {"path": "` + 170 + `", "product_count": {"gt": 0}}}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer 62822592-2423-448a-ad97-e13b7a789eee'
            }
        }).then((res) => {
            dispatch(CAT_LOAD_PRODUCTS_SUCCESS(res))
        }).catch((err) => {
            dispatch(CAT_LOAD_PRODUCTS_FAILURE(err))
        })
    }
}
