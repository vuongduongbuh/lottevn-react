import * as category from './category.actions';
import { createReducer } from 'redux-act';

const initialState = {
    loading: false,
    products: [],
    error: {}
}

const handlers = {
    [category.CAT_LOAD_PRODUCTS]: (state) => ({
        ...state,
        loading: true
    }),
    [category.CAT_LOAD_PRODUCTS_SUCCESS]: (state, payload) => ({
        ...state,
        loading: false,
        products: payload
    }),
    [category.CAT_LOAD_PRODUCTS_FAILURE]: (state, err) => ({
        ...state,
        loading: false,
        error: err
    })
}

const reducer = createReducer(handlers, initialState);

export default reducer