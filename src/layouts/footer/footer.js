import React from 'react';

export default class Footer extends React.Component {
    render() {
        return (
            <div className="footer-lt">
                <section className="subscrible-section">
                    <div className="container-lt">
                        <h2><span>BẠN LÀ KHÁCH HÀNG MỚI?</span></h2>
                        <p className="subtitle">Đăng ký email và nhận mã giảm giá <strong>50.000 ₫</strong>.</p>
                    </div>
                </section>
            </div>
        )
    }
}
