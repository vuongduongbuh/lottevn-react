import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class HeaderMainMenu extends React.Component {
    render() {
        return (
            <nav className="navigation-top">
                <div className="container-lt">
                    <div className="megamenu">
                        <Link className="mega-text" to={{ pathname: "/" }}>
                            <span className="icon-bar">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                            <span className="txt">Tất cả danh mục</span>
                        </Link>
                    </div>
                </div>
            </nav>
        )
    }
}