import React from 'react';

export default class HeaderTopBar extends React.Component {
    render() {
        return (
            <section className="topbar">
                <div className="container-lt">
                    <ul className="topbar-nav">
                        <li>
                            <a title="Bán hàng cùng Lotte" href="https://seller.lotte.vn/vendor/popVendorRegi">Bán hàng cùng Lotte</a>
                        </li>
                        <li>
                            <a href="/campaign/download-app/" title="Tải App">Tải App</a>
                        </li>
                    </ul>
                </div>
            </section>
        )
    }
}