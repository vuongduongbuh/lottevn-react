import React from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { login, loadUserInfo, logout } from '../../../store/auth/auth.actions';

class HeaderSearchBar extends React.Component {
    constructor(props) {
        super(props);

        this.login = this.login.bind(this);
        this.loginSuccess = this.loginSuccess.bind(this);
        this.logout = this.logout.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.isLoggedIn && prevProps.isLoggedIn !== this.props.isLoggedIn) {
            this.loginSuccess();
        }
    }

    login() {
        this.props.login({ username: 'm@a.com', password: 'admin123' });
    }

    logout() {
        this.props.logout();
    }

    loginSuccess() {
        this.props.loadUserInfo();
    }
    render() {
        return (
            <header className="header">
                <div className="container-lt">
                    <Link className="logo-lotte" to={{ pathname: "/" }}>
                        <img alt="Lotte.vn" src="https://d1710i1dsqwesz.cloudfront.net/media/logo/default/lotte-logo-2_1.png" />
                    </Link>

                    <ul className="account-block">
                        <li className={'myaccount-menu' + (this.props.isLoggedIn ? ' active' : '')}>
                            <a>
                                {this.props.isLoggedIn ? <span className="icon icon-myaccount"></span> : <span onClick={this.login} className="icon icon-myaccount"></span>}
                            </a>
                            <div className="dropdown-wrap myaccount-view" style={!this.props.isLoggedIn ? { display: 'none' } : {}}>
                                <div className="content">
                                    <div className="head-info">
                                        <span className="welcome">{`${this.props.userInfo.firstname} ${this.props.userInfo.lastname}`}</span>
                                        <a className="signout-link" onClick={this.logout} title="Đăng xuất">Đăng xuất</a>
                                    </div>
                                    <ul className="account-menu">
                                        <li>
                                            <a routerlink="/account/orders" title="Danh sách đơn hàng" href="/account/orders">Danh sách đơn hàng</a>
                                        </li>
                                        <li>
                                            <a routerlink="/account" title="Quản lý tài khoản" href="/account">Quản lý tài khoản</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li className="wishlist-menu">
                            <a routerlink="/account/wishlist" href="/account/wishlist">
                                <span className="icon icon-wishlist"></span>
                            </a>
                        </li>

                        <li className="cart-menu active">
                            <a href="/checkout/cart">
                                <span className="icon icon-cart"></span>
                                <span className="number">20</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </header>
        )
    }
}

const mapStateToProps = state => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        userInfo: state.auth.userInfo
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ login, loadUserInfo, logout }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HeaderSearchBar)