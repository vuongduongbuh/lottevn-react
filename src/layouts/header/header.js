import React from 'react';

import HeaderTopBar from './components/topbar';
import HeaderSearchBar from './components/searchbar';
import HeaderMainMenu from './components/mainmenu';
export default class Header extends React.Component {
    render() {
        return (
            <div>
                <HeaderTopBar />
                <section className="sticky-header">
                    <HeaderSearchBar />
                    <HeaderMainMenu />
                </section>
            </div>
        )
    }
}
