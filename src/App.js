import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import AppRouter from './AppRouter';
import Footer from './layouts/footer/footer';
import Header from './layouts/header/header';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <Router>
      <div>
        <Header />
        <AppRouter />
        <Footer />
      </div>
    </Router>
  }
}

