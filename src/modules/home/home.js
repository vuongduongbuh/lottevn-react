import React from 'react';

import HomeTopTrending from "./components/trending";

export default class Home extends React.Component {
    render() {
        return (
            <HomeTopTrending />
        );
    }
}