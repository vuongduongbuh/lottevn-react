import React from 'react';

export default class HomeTopTrending extends React.Component {
    render() {
        return (
            <section className="highlight-tpo">
    <div className="container-lt clearfix">
        <div className="left-content"><p><a className="tpo-item" title="#cungnhauvaobep" href="https://www.lotte.vn/xu-huong/chi-tiet/cung-nhau-vao-bep"> <span className="field-img"> 
        <img title="#cungnhauvaobep" src="https://d1710i1dsqwesz.cloudfront.net/media/banner/_/c/_cungnhauvaobep.jpg" alt="" width="480" height="480" /> <span className="view-info"><strong>530</strong> Xem &nbsp;|&nbsp; <strong>101</strong> Thích</span> </span> <span className="field-title">Xu hướng mới nhất</span> <span className="field-name">#cungnhauvaobep</span> <span className="field-desc">Căn bếp là nơi gia đình quây quần ăn uống, trò chuyện. Nhóm lửa căn bếp không chỉ là công việc riêng của người phụ nữ, người vợ, người mẹ mà là của mọi thành viên trong gia đình. Hãy cùng san sẻ công việc, cùng nhau vào bếp nấu những món ăn ngon, gắn kết tình thân!</span> </a></p></div>
        <div className="right-content">
            <div><p className="field-title">Xu hướng nổi bật trong tuần</p>
<h2>"NGÀY CỦA BẢN THÂN"</h2>
<p>Đôi khi trong cuộc sống bạn thường chỉ nghĩ đến việc làm thế nào để khiến cho những người xung quanh vui vẻ và hạnh phúc nhưng lại quên mất một điều rằng hơn ai hết bản thân bạn cũng nên và cần được yêu thương. Mua sắm để tự thưởng và làm cho bản thân đẹp hơn mỗi ngày bạn nhé!</p>
<p className="read-more"><a className="button-border" href="/xu-huong/tong-hop">Xem thêm</a></p></div>
            <div className="tab-wrap">
                <ul className="tab-menu">
                    <li className="">
                        <a title="Đang Hot">
                            <span>Đang Hot</span>
                        </a>
                    </li><li className="active">
                        <a title="Quan tâm nhiều nhất">
                            <span>Quan tâm nhiều nhất</span>
                        </a>
                    </li><li>
                        <a title="Mới cập nhật">
                            <span>Mới cập nhật</span>
                        </a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="content hidden">
                        <ul className="tpo-list">
                            <li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/thuc-pham-han-quoc">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_2" title="#thucphamhanquoc" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/t/_thucphamhanquoc.jpg" />
                                        <span className="view-info">
                                            <strong>329</strong> Xem &nbsp;|&nbsp;
                                            <strong>101</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#thucphamhanquoc</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/thoi-bay-con-nong">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_3" title="#thoibayconnong" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/t/_thoibayconnong.png" />
                                        <span className="view-info">
                                            <strong>606</strong> Xem &nbsp;|&nbsp;
                                            <strong>80</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#thoibayconnong</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/phu-kien-mua-he">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_4" title="#phukienmuahe" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/p/h/phukienmuahe.jpg" />
                                        <span className="view-info">
                                            <strong>834</strong> Xem &nbsp;|&nbsp;
                                            <strong>126</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#phukienmuahe</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/ngoi-nha-cong-nghe">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_5" title="#ngoinhacongnghe" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/n/_ngoinhacongnghe.png" />
                                        <span className="view-info">
                                            <strong>188</strong> Xem &nbsp;|&nbsp;
                                            <strong>100</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#ngoinhacongnghe</span>
                                </a>
                            </li>
                        </ul>
                    </div><div className="content">
                        <ul className="tpo-list">
                            <li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/phu-kien-mua-he">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_2" title="#phukienmuahe" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/p/h/phukienmuahe.jpg" />
                                        <span className="view-info">
                                            <strong>834</strong> Xem &nbsp;|&nbsp;
                                            <strong>126</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#phukienmuahe</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/game-thu">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_4" title="#gamethu" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/g/_gamethu.jpg" />
                                        <span className="view-info">
                                            <strong>520</strong> Xem &nbsp;|&nbsp;
                                            <strong>231</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#gamethu</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/an-vat-van-phong">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_6" title="#anvatvanphong" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/a/_anvatvanphong.jpg" />
                                        <span className="view-info">
                                            <strong>147</strong> Xem &nbsp;|&nbsp;
                                            <strong>231</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#anvatvanphong</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/cam-nang-doi-song">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_8" title="#camnangdoisong" src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/c/a/camnangdoisong.jpg" />
                                        <span className="view-info">
                                            <strong>489</strong> Xem &nbsp;|&nbsp;
                                            <strong>138</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#camnangdoisong</span>
                                </a>
                            </li>
                        </ul>
                    </div><div className="content hidden">
                        <ul className="tpo-list">
                            <li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/da-khoe-chao-he">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_2" title="#dakhoechaohe" data-src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/d/_dakhoechaohe_1.jpg" />
                                        <span className="view-info">
                                            <strong>515</strong> Xem &nbsp;|&nbsp;
                                            <strong>59</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#dakhoechaohe</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/tap-the-thao-de-dang">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_5" title="#tapthethaodedang" data-src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/t/a/tapthethaodedang.jpg" />
                                        <span className="view-info">
                                            <strong>164</strong> Xem &nbsp;|&nbsp;
                                            <strong>39</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#tapthethaodedang</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/ao-thun-giai-nhiet">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_8" title="#aothungiainhiet" data-src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/a/o/aothungiainhiet.jpg" />
                                        <span className="view-info">
                                            <strong>406</strong> Xem &nbsp;|&nbsp;
                                            <strong>89</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#aothungiainhiet</span>
                                </a>
                            </li><li>
                                
                                <a className="tpo-item" href="/xu-huong/chi-tiet/maxi-mua-he">
                                
                                    <span className="field-img">
                                        <img alt="" className="img-banner" width="164" data-layer="Home_Z2_11" title="#maximuahe" data-src="https://d1710i1dsqwesz.cloudfront.net/media/resized/banner/_/m/_maximuahe.png" />
                                        <span className="view-info">
                                            <strong>563</strong> Xem &nbsp;|&nbsp;
                                            <strong>90</strong> Thích
                                        </span>
                                    </span>
                                    <span className="field-name">#maximuahe</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="content hidden">
                        Popular
                    </div>
                    <div className="content hidden">
                        New Arrival
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        );
    }
}